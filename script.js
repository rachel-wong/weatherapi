// First get longitide and latitude from our location (native to javascript)

// after page is loaded, get the location
window.addEventListener('load', () => {
  let long, lat
  let temperatureDescription = document.querySelector('.temperature-description')
  let temperatureDegree = document.querySelector('.temperature-degree')
  let locationTimeZone = document.querySelector('.location-timezone')
  let temperatureSection = document.querySelector('.temperature-section')
  let temperatureSpan = document.querySelector('.temperature-section span')

  // if navigator geolocation exists (which it does on browsers), then we find the exact location of the user.
  // when page loads, user will be prompted by browser whether to disclose location 
  if (navigator.geolocation) {
    console.log(navigator.geolocation)
    // navigator.geolocation now takes a longer time on chrome to fetch
    navigator.geolocation.getCurrentPosition(position => {
      console.log(position)
      long = position.coords.longitude
      lat = position.coords.latitude
      // let key = DARK_SKY_API_KEY
      const proxy = 'https://cors-anywhere.herokuapp.com/'
      const api = `${proxy}https://api.darksky.net/forecast/174721d8a6149d80555bac44119df0d9/${lat},${long}`
      console.log("Just before fetching")
      // this do a GET request
      fetch(api)
        .then(response => {
          console.log("response headers", response.headers)
          // response => returns promise with everything + headers
          // response.json =? returns promise with just the body - headers
          console.log("first response at 1st then", response)
          return response.json()
        })
        .then(data => {
          // unwraps the response object -headers into Javascript object here
          // destructure from the data object under the currently properties
          console.log(data)
          const {
            temperature,
            summary,
            icon
          } = data.currently // .currently is an object in the object that is returned

          // set the data form the API onto the DOM elements
          temperatureDegree.textContent = temperature
          temperatureDescription.textContent = summary
          locationTimeZone.textContent = data.timezone
          // convention is: setIcons(icon from the api, id of the dom element)
          setIcons(icon, document.querySelector(".weatherIcon")) // always remember the . and #

          // Change DOM temperature to Celsius/Fahrenheit by clicking on the temperature  section
          // Check if fahrenheit or celsius
          temperatureSection.addEventListener('click', () => {

            // if displayed as fahrenheit, changed to degree celsius
            if (temperatureSpan.textContent === "F") {
              temperatureSpan.textContent = "C"
              temperatureDegree.textContent = Math.floor((temperature - 32) * (5 / 9))
            } else {
              temperatureSpan.textContent = "F"
              temperatureDegree.textContent = Math.floor(temperature)
            }
          })
        }).catch(err => {
          // err is an object here
          console.log(err.message)
        })
    })
  } else {
    alert("this is not working because geolocation tracking disallowed by user.")
  }

  // set a skycon from library according to the API data returned
  function setIcons(icon, iconID) {
    const skycons = new Skycons({
      color: "white"
    }) // initiate the skycons library
    const currentIcon = icon.replace(/-/g, "_").toUpperCase() // format the icon name from the API data to the format acceptable to the skycons
    return skycons.set(iconID, Skycons[currentIcon]) // change the skycons  (see the skycons docs)
  }
})