## Weather API in Vanilla JS

> Because we should all do this at least once in our lives - like year 12 or chickenpox.

![boring weather app](screenshot.png)

### API 
The data of choice is the free, emotionally-uncomplicated :snowflake: :snowflake: **Dark Sky API** :snowflake: :snowflake: .

https://darksky.net/

### Some observations :eyes:

* I did not solve that hide-yo-APIKey issue at all. Marked unresolved :see_no_evil:

* Everything is wrapped inside `navigator.geolocation.getCurrentPosition`. I'm not convinced that is a good thing. Essentially once the page loads, the user is prompted to share their location. Once a location is in place, then the fetch API call is made to Dark Sky. 

* What fetch .then returns
```
fetch(api_URL)
  .then(here it sreturns a response object with headers which you then remove with .json)
  .then(here it returns a javascriptobject here which you use to display the page. This is essentially where the whole program happens.)
```

![what .then returns](output.png)

### Other Resources that I used
The animated icons are from [skycons](https://github.com/darkskyapp/skycons)
- you need to download the `.js` file locally
- you need to link to the skycon javascript in the HTML
- you need to initialise the library as per the documentation
- then `setIcons` to it depending to the weather label returned from the Dark Sky API json

[CORS Anywhere](https://cors-anywhere.herokuapp.com/) easiest way to get around the CORS problem (accessing the API from localhost). Just peg it to the front of your URL. 

[Reference](https://www.youtube.com/watch?v=wPElVpR1rwA&list=PLQvEmJEqC3ycLRyJ4-IDWVybNqoOWd72J&index=23&t=0s)